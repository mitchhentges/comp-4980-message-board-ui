var apiBase = 'http://messageboard.fuzzlesoft.ca/api/';

function get(url) {
    return new Promise(function(resolve, reject) {
        var req = new XMLHttpRequest();
        req.addEventListener("load", function() {
            if (this.status < 400) {
                resolve(JSON.parse(this.responseText))
            } else {
                console.error(this);
                reject(this.responseText);
            }
        });
        req.open("GET", url);
        req.send();
    });
}

function del(url) {
    return new Promise(function(resolve, reject) {
        var req = new XMLHttpRequest();
        req.addEventListener("load", function() {
            if (this.status < 400) {
                resolve(JSON.parse(this.responseText))
            } else {
                console.error(this);
                reject(this.responseText);
            }
        });
        req.open("DELETE", url);
        req.send();
    });
}

function post(url, data) {
    return new Promise(function(resolve, reject) {
        var req = new XMLHttpRequest();
        req.addEventListener("load", function() {
            if (this.status < 400) {
                resolve(JSON.parse(this.responseText))
            } else {
                console.error(this);
                reject(this.responseText);
            }
        });
        req.open("POST", url);
        req.send(data);
    });
}

var removeListeners = [];
function refreshMessages() {
    // Show current messages
    get(apiBase + 'messages').then(function(messages) {
        var ulNode = document.querySelector('.message-list ul');
        removeListeners.forEach(function(removeListener) { removeListener() });
        while (ulNode.firstChild) {
            ulNode.removeChild(ulNode.firstChild);
        }

        removeListeners = messages.map(function(message) {
            var liNode = document.createElement('li');
            liNode.title = 'Click to remove';
            liNode.appendChild(document.createTextNode(message.message));
            ulNode.appendChild(liNode);

            function onClick() {
                del(apiBase + 'messages/' + message.uuid).then(refreshMessages);
            }
            liNode.addEventListener('click', onClick);
            return function() {
                liNode.removeEventListener('click', onClick);
            }
        });
    });
}
refreshMessages();

// Submit new message
var pushMessage = document.querySelector(".message-list input");
pushMessage.addEventListener('keypress', function(e) {
    if (e.keyCode !== 13) {
        return;
    }

    var message = e.target.value;
    e.target.value = '';
    post(apiBase + 'messages', message).then(function() {
        console.log("Submitted message", message);
        refreshMessages();
    });
});

// Submit new alert
var submitAlert = document.querySelector(".message-alert input");
submitAlert.addEventListener('keypress', function(e) {
    if (e.keyCode !== 13) {
        return;
    }

    var alert = e.target.value;
    e.target.value = '';
    post(apiBase + 'submitAlert', alert).then(function() {
        console.log("Submitted alert", alert);
    });
});

var eventTypeSelect = document.querySelector('.event-type-select');

// Get list of event types
get(apiBase + 'eventTypes').then(function(eventTypes) {
    eventTypes.forEach(function addToDom(eventType) {
        var optionNode = document.createElement('option');
        optionNode.value = eventType;
        optionNode.appendChild(document.createTextNode(eventType));
        eventTypeSelect.appendChild(optionNode);
    });
    if (eventTypeSelect.firstChild) {
        eventTypeSelect.value = eventTypeSelect.firstChild.value;
        eventTypeChanged();
    }
});

// Render graph of data
var chart = undefined;
function eventTypeChanged() {
    get(apiBase + 'events/' + eventTypeSelect.value).then(function(eventGroups) {
        var datasets = [];
        for (var key in eventGroups) {
            if (eventGroups.hasOwnProperty(key)) {
                datasets.push({
                    label: key,
                    fill: false,
                    data: eventGroups[key].map(function(event) {
                        return {
                            x: new Date(event.date),
                            y: event.value,
                            context: event.context
                        }
                    }),
                    lineTension: 0,
                    borderColor: toColor(key),
                });
            }
        }

        if (chart) {
            chart.destroy();
        }

        var ctx = document.getElementById('chart').getContext('2d');
        chart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: datasets
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var text = [];
                            var localData = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            for (var key in localData.context) {
                                if (localData.context.hasOwnProperty(key)) {
                                    text.push(key + ':' + localData.context[key]);
                                }
                            }
                            console.log(text);
                            return text;
                        }
                    }
                },
                hover: {
                    intersect: false
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        position: 'bottom'
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    });
}
eventTypeSelect.addEventListener('change', eventTypeChanged);


// Misc
function toColor(str){
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }

    var c = (hash & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return '#' + ("00000".substring(0, 6 - c.length) + c);
}